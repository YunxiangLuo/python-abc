import dlib
import cv2
import numpy as np

# video_capture = cv2.VideoCapture("./video/demo_clip.mp4")
video_capture = cv2.VideoCapture(0)

detector = dlib.get_frontal_face_detector()
win = dlib.image_window()

while video_capture.isOpened():
    # Grab a single frame of video
    ret, frame = video_capture.read()  # 逐帧读取图片

    if not ret:
        break

    # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
    frame = frame[:, :, ::-1]  # BGR格式转成RGB格式

    image = np.array(frame)
    detector_face = detector(image, 1)
    win.clear_overlay()
    win.set_image(image)
    win.add_overlay(detector_face)

video_capture.release()
cv2.destroyAllWindows()