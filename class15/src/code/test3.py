import dlib
import numpy as py
from PIL import Image

path = "./people_i_know/figure_1.jpg"
detector = dlib.get_frontal_face_detector()
win = dlib.image_window()

image = py.array(Image.open(path))
dets, sco, idx = detector.run(image, 0, 0.5)  # 设置阈值
for i, d in enumerate(dets):
    print('%d：score %f, face_type %f' % (i, sco[i], idx[i]))

win.clear_overlay()
win.set_image(image)
win.add_overlay(dets)
dlib.hit_enter_to_continue()